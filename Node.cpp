#include "Node.h"

Node::Node(int x){ 						//Constructor
	this->x = x; 
	next = NULL; 
}


Node::~Node(){}							//Empty destructor


Node* Node::getNext() { 				//Get ptr to next entry
	return next; 
}


void Node::setNext(Node * node){ 		//Set address to next entry
	next = node; 
}


int Node::getData(){ 					//Get data
	return x; 
}


void Node::setData(int data){			//Set data
	x = data;
}


bool Node::operator < (Node * node){	//Overload <, used to sord data
	return (x < node->x);
}


bool Node::operator >(Node * node){		//Overload >, used to sort data
	return (x > node->x);
}