#pragma once
#include <cstdlib>

class Node{							//A node in a list
private:
	int x;							//Data to be stored
	Node * next;					//Poointer to next entry in list
public:
	Node(int x);					//Constructor
	~Node();						//Empty destructor
	Node* getNext();				//Get ptr to next entry
	void setNext(Node*);			//Set address to next entry
	int  getData();					//Get data
	void setData(int);				//Set data
	bool operator < (Node*);		//Overload <, used to sord data
	bool operator >(Node*);			//Overload >, used to sort data
};