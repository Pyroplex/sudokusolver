﻿#include "Board.h"


Board::Board(){																			//Constructor
	numLeft = BOARDSIZE * BOARDSIZE;
	for (int i = 0; i < BOARDSIZE; i++)													//Set all List ptrs to NULL
		for (int j = 0; j < BOARDSIZE; j++)
			board[i][j] = NULL;

}


Board::~Board(){																		//Deletes all lists in array
	for (int i = 0; i < BOARDSIZE; i++)
		for (int j = 0; j < BOARDSIZE; j++)
			if (board[i][j]) delete board[i][j];										//!NULL
}

void Board::newBoard(){																	//get numbers from user
	int temp;																			//temp storage for input
	cout << "\n\tType corresponding numbers ( 0 - " << BOARDSIZE << "):\n";				//print range

	for (int i = 0; i < BOARDSIZE; i++)													//for all lists
		for (int j = 0; j < BOARDSIZE; j++){
			do{																			//while incorrect input
				cout << "\t\tROW " << (i + 1) << "\tCOL " << (j + 1) << " : \t";
				cin >> temp; cin.ignore();												//get input
				cout << '\n';
			} while ((temp < 0) && (temp > BOARDSIZE));									//check input
			if (temp){
				board[i][j] = new List(ASYNC, temp, true);								//create list if good
				numLeft--;																//count down numbers to check
			}
		}
}
/*
	201: ╔		205: ═		203: ╦		187: ╗
	
	186: ║		
	
	204: ╠					206: ╬		185: ╣

	200: ╚					202: ╩		188: ╝
*/

void Board::display(){																	//prints board
	int temp;
	cout << "\n\t";
	for (int i = 0; i < BOARDSIZE; i++){												//display top border
		if (!i) cout << char(201) << char(205);											//first corner
		else if (i == (BOARDSIZE - 1))
			cout << char(203) << char(205) << char(187) << '\n';
		else
			cout << char(203) << char(205);
	}

	for (int i = 0; i < BOARDSIZE; i++){
		cout << '\t';
		for (int j = 0; j < BOARDSIZE; j++){
			cout << char(186); 
			if (board[i][j]){
				temp = board[i][j]->getData();
				if (temp) cout << temp;
				else cout << ' ';
			}
			else
				cout << ' ';
			if (j == (BOARDSIZE - 1)) cout << char(186);
		}
		cout << '\n';

		if (i != (BOARDSIZE - 1)){
			for (int k = 0; k < BOARDSIZE; k++){										//display top border
				if (!k) cout << '\t' << char(204) << char(205);							//first corner
				else if (k == (BOARDSIZE - 1))
					cout << char(206) << char(205) << char(185) << '\n';
				else
					cout << char(206) << char(205);
			}
		}
	}

	for (int i = 0; i < BOARDSIZE; i++){												//display top border
		if (!i) cout << '\t' << char(200) << char(205);											//first corner
		else if (i == (BOARDSIZE - 1))
			cout << char(202) << char(205) << char(188) << '\n';
		else
			cout << char(202) << char(205);
	}
	if(DEBUG) cout << "Remaining numbers to calculate: " << numLeft << '\n';
}


bool Board::isInRow(const int y, const int data){										//returns true if number is in the same row
	for (int i = 0; i < BOARDSIZE; i++)													//all elements in row
		if (board[y][i])																//board exists
			if (board[y][i]->isDone() && (board[y][i]->getData() == data)) return true;	//data match/only one possibility
	return false;																		//No match	
}


bool Board::isInCol(const int x, const int data){										//returns true if number is in the same column
	for (int i = 0; i < BOARDSIZE; i++)													//all elements in col
		if (board[i][x])																//board exists
			if (board[i][x]->isDone() && (board[i][x]->getData() == data)) return true;	//data match/only one possibility
	return false;																		//No match	
}


void Board::getBlockCoords(const int y, const int x, int & y1, int & y2, int & x1, int & x2){	//Specifies the block for a given coordinate
	//y - coordinates
	if (y < 3){																			//Top row
		y1 = 0; y2 = 2;
	}
	else if (y > 5){																	//Bottom row
		y1 = 6; y2 = 8;
	}
	else{																				//Middle row
		y1 = 3; y2 = 5;
	}
	//x - coordinates
	if (x < 3){																			//Left col
		x1 = 0; x2 = 2;
	}
	else if (x > 5){																	//Rigth col
		x1 = 6; x2 = 8;
	}
	else{																				//Middle col
		x1 = 3; x2 = 5;
	}
}


bool Board::isInBlock(const int y, const int x, const int data){						//returns true if number is in the same block
	int y1, y2, x1, x2;																	//coordinates to define block

	getBlockCoords(y, x, y1, y2, x1, x2);												//retrieve coordinates for block

	for (int i = y1; i <= y2; i++)														//all elements in row
		for (int j = x1; j <= x2; j++)													//all elements in col
			if (board[i][j])															//if exists
				if (board[i][j]->isDone() && (board[i][j]->getData() == data)) return true;//data match/only one possibility
	return false;																		//No match
}


void Board::blockBuster(){																		//Checks each block and assigns values that has only one spot
	int ant = 0;																		//Number of appearances
	int y1, y2, x1, x2;																	//Coordinates to define block
	
	for (int i = 1; i <= BOARDSIZE; i++){												//all possible values 
		for (int j = 0; j < BOARDSIZE; j++){											//all blocks
			switch (j){																	//Set coordinates to define block
			case 0: y1 = 0; x1 = 0; y2 = 2; x2 = 2; break;
			case 1: y1 = 0; x1 = 3; y2 = 2; x2 = 5; break;
			case 2: y1 = 0; x1 = 6; y2 = 2; x2 = 8; break;
			case 3: y1 = 3; x1 = 0; y2 = 5; x2 = 2; break;
			case 4: y1 = 3; x1 = 3; y2 = 5; x2 = 5; break;
			case 5: y1 = 3; x1 = 6; y2 = 5; x2 = 8; break;
			case 6: y1 = 6; x1 = 0; y2 = 8; x2 = 2; break;
			case 7: y1 = 6; x1 = 3; y2 = 8; x2 = 5; break;
			case 8: y1 = 6; x1 = 6; y2 = 8; x2 = 8; break;
			}
																		//Loop through all elements in block
			for (int y = y1; y <= y2; y++){												//y - axis
				for (int x = x1; x <= x2; x++){											//x - axis
					if (board[y][x]->isSet(i))											//If this value is in list
						ant++;															//increase number of appearances
				}
			}

			if (ant == 1){																//update field if only one element in block contains value
				for (int y = y1; y <= y2; y++){											//y - axis
					for (int x = x1; x <= x2; x++){										//x - axis
						if (board[y][x]->isSet(i) && !board[y][x]->isDone()){			//If this value is in list
							board[y][x]->destroy();										//remove all items in list
							board[y][x]->add(i);										//Add element again
							board[y][x]->setDone();										//mark as determined
							numLeft--;
						}
					}
				}
			}
			ant = 0;
		}
	}

}


void Board::rowBuster(){																//Checks each row and assigns values that has only one spot
	int ant = 0;
	int y1, y2, x1, x2;																	//coords for block (for cleanup)

	for (int i = 1; i <= BOARDSIZE; i++){												//all possible values
		for (int y = 0; y < BOARDSIZE; y++){											//y - axis
			for (int x = 0; x < BOARDSIZE; x++){										//x - axis
				if (board[y][x]->isSet(i))												//if element contains value
					ant++;																//increase number of appearances
			}

			if (ant == 1){
				for (int j = 0; j < BOARDSIZE; j++){									//x - axis
					if (board[y][j]->isSet(i) && !board[y][j]->isDone()){				//If this value is in list end element not done
						board[y][j]->destroy();
						board[y][j]->add(i);
						board[y][j]->setDone();
						numLeft--;

						getBlockCoords(y, j, y1, y2, x1, x2);

						//Update block
						for (int k = y1; k <= y2; k++)											//y - axis
							for (int l = x1; l <= x2; l++)										//x - axis
								if (board[k][l]->isSet(i) && !board[k][l]->isDone())			//If this value is in list
									board[k][l]->remove(i);										//remove all items in list				
					}
				}
			}
			ant = 0;
		}
	}
}


void Board::colBuster(){																		//Checks each column and assigns values that has only one spot
	int ant = 0;
	int y1, y2, x1, x2;																	//coords for block (for cleanup)

	for (int i = 1; i <= BOARDSIZE; i++){												//all possible values
		for (int x = 0; x < BOARDSIZE; x++){											//x - axis
			for (int y = 0; y < BOARDSIZE; y++){										//y - axis
				if (board[y][x]->isSet(i))												//if element contains value
					ant++;																//increase number of appearances
			}

			if (ant == 1){
				for (int j = 0; j < BOARDSIZE; j++){									//x - axis
					if (board[j][x]->isSet(i) && !board[j][x]->isDone()){				//If this value is in list end element not done
						board[j][x]->destroy();
						board[j][x]->add(i);
						board[j][x]->setDone();
						numLeft--;

						getBlockCoords(j, x, y1, y2, x1, x2);

						//Update block
						for (int k = y1; k <= y2; k++)											//y - axis
							for (int l = x1; l <= x2; l++)										//x - axis
								if (board[k][l]->isSet(i) && !board[k][l]->isDone())			//If this value is in list
									board[k][l]->remove(i);										//remove all items in list				
					}
				}
			}
			ant = 0;
		}
	}
}


void Board::calculatePossibilities(){													//Generates a list of possible values for each field
	for (int i = 0; i < BOARDSIZE; i++)													//y-axis
		for (int j = 0; j < BOARDSIZE; j++){											//x-axis
			for (int k = 1; k <= BOARDSIZE; k++){										//All possible values for field
				if (!isInRow(i, k) && !isInCol(j, k) && !isInBlock(i, j, k)){			//not present in row, col or block

					if (board[i][j]){													//exists
						if (!board[i][j]->isDone())										//is not already determined
							board[i][j]->add(k);										//	then add number to list
					}
					else                                                                //doesn't exist
						board[i][j] = new List(ASYNC, k, false);						//create new list

				}
			}
			if (board[i][j]->getLength() == 1 && !board[i][j]->isDone()){											//only one possibility
				board[i][j]->setDone();
				numLeft--;
			}

		}
}


void Board::solve(){										//solves the puzzle
	int length = 0, data, prev = 0;							//length of list for each item

	cout << "\tCALCULATING POSSIBILITIES\n";

	calculatePossibilities();								//Generates a list for all fields with possible values
	if (DEBUG){
		cout << "\nALL POSSIBILITIES FOR EACH FIELD:\n";
		for (int i = 0; i < BOARDSIZE; i++)
			for (int j = 0; j < BOARDSIZE; j++){
			cout << "\tROW " << i << "\tCOL " << j << ":\t";
			if (board[i][j]) board[i][j]->display();
			cout << '\n';
			}
		cout << "\n\n\tBOARD AFTER LISTGENERATION:\n";

		display();
	}
	cout << "\n\n\tSOLVING!\n\n";
	while (numLeft){											//still numbers to solve
		if (prev == numLeft){									//No update since last cycle
			if(DEBUG) cout << "\n\nNO UPDATE!!! RUNNING BUSTER\n\n";
			blockBuster();										//comparing elements in each block
			rowBuster();										//Comparing elements in each row
			colBuster();										//comparing elements in each column
		}
		prev = numLeft;
		for (int i = 0; i < BOARDSIZE; i++){					//y-axis
			for (int j = 0; j < BOARDSIZE; j++){				//x-axis

				if (!board[i][j]->isDone()){					//If field not already done
					length = board[i][j]->getLength();			//Get length of list at current field
					for (int k = 1; k <= length; k++){			//All items in list
						data = board[i][j]->getData(k);			//retrieve possible value

						if (data && (isInRow(i, data) || isInCol(j, data)//Is present in row, col or block
							|| isInBlock(i, j, data))){

							board[i][j]->setData(k, 0);			//This data is no longer a possibility
						}
					}
					board[i][j]->remove(0);						//Remove all elements no longer a possibility
					length = board[i][j]->getLength();			//Get updated length for list in field
					if (length == 1){							//Only one item left
						board[i][j]->setDone();					//	then this is the final number
						numLeft--;								//Decrease numbers left to solve
					}
				}

			}
		}
		if (DEBUG){
			cout << '\n';
			display();
			cout << '\n';
		}
	}
	cout << "\n\n\tFINAL BOARD:\n";
	display();
}


void Board::test(){
	/*int testBoard[] = { 7, 0, 5,   8, 6, 4,   0, 0, 9,		//EASY
						9, 0, 2,   0, 1, 0,   6, 0, 5, 
						0, 1, 0,   0, 5, 0,   0, 0, 8, 
						
						0, 0, 0,   7, 0, 0,   0, 0, 0, 
						2, 0, 0,   6, 8, 1,   0, 0, 4, 
						0, 0, 0,   0, 0, 3,   0, 0, 0, 
						
						8, 0, 0,   0, 7, 0,   0, 1, 0, 
						3, 0, 6,   0, 9, 0,   2, 0, 7, 
						1, 0, 0,   4, 3, 5,   9, 0, 6 
	}, index = 0, temp;
	int testBoard[] = { 0, 0, 0,   0, 0, 5,   0, 7, 9,			//EVIL
						1, 3, 0,   6, 0, 0,   0, 0, 0,			//		Foreløpig uløyselig
						0, 0, 0,   8, 0, 0,   0, 1, 0,			//		da en må "tippe"løsning

						8, 0, 0,   5, 0, 2,   0, 0, 6,
						0, 0, 7,   0, 0, 0,   3, 0, 0,
						6, 0, 0,   7, 0, 3,   0, 0, 1,

						0, 7, 0,   0, 0, 4,   0, 0, 0,
						0, 0, 0,   0, 0, 8,   0, 4, 2,
						4, 2, 0,   9, 0, 0,   0, 0, 0
	}, index = 0, temp;
*/
	int testBoard[] = { 4, 0, 9,   5, 0, 2,   0, 0, 0,			//HARD
						0, 0, 0,   0, 0, 0,   0, 0, 0,			
						0, 7, 0,   0, 1, 0,   0, 6, 0,			

						0, 1, 7,   4, 0, 0,   3, 0, 8,
						8, 0, 0,   0, 2, 0,   0, 0, 6,
						9, 0, 2,   0, 0, 7,   5, 4, 0,

						0, 5, 0,   0, 9, 0,   0, 8, 0,
						0, 0, 0,   0, 0, 0,   0, 0, 0,
						0, 0, 0,   6, 0, 4,   7, 0, 9
	}, index = 0, temp;  

	if (DEBUG){
		cout << "\tTESTING FUNCTIONS!\n\tPRINTING BOARD AFTER INIT\n\n";
		display();
	}


	for (int i = 0; i < BOARDSIZE; i++)
		for (int j = 0; j < BOARDSIZE; j++){
			temp = testBoard[index++];
			if (temp){
				board[i][j] = new List(ASYNC, temp, true);
				numLeft--;
			}
		}

	if(DEBUG) cout << "\tPRINTING BOARD AFTER NUMBERS ADDED\n\n";
	display();

	solve();
}