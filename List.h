#pragma once
#include <iostream>									//cin, cout
#include "Node.h"
using namespace std;

enum Type { LIFO, FIFO, ASYNC, DESYNC };				//List-types

class List{						//container class, manages all entries in list
private:
	Type type;					//LIFO, FIFO, ASYNC or DESYNC
	Node* first, *last;			//Ptr to first and last entry
	bool done;					//If number is determined
	int length;					//number of nodes in list
public:
	List(Type type);			//Constructor, defines list type
	List(Type type, int data, bool finished);//Constructor, defines list type and adds first element
	~List();					//Destructor, deallocates memory
	void add(int x);			//Add data to list
	void display();				//Write list to screen
	int  getLength();			//get length of list
	int  getData();				//Returns the value of field if only one is present
	int  getData(int index);	//Returns the value of Node at index
	void setData(int index, int val);//Update data at index

	void remove(int data);		//Removes items that == "data" from the list
	void destroy();				//Removes ALL items in list
	bool isSet(int data);		//returns true if already in list
	bool isDone();				//Returns true if the number is determined
	void setDone();				//Specifies number as done
};