#include "List.h"


List::List(Type type){ 							//Constructor, defines list type
	this->type = type;							//set listtype
	first = NULL; last = NULL;					//initialize pointers to NULL
	length = 0;									//No elements
	done = false;								//Not done
}


List::List(Type type, int data, bool d){		//Constructor, defines list type and adds first elementList(Type type, int data);	//Constructor, defines list type and adds first element
	this->type = type;							//Set listtype
	first = NULL; last = NULL;					//initialize pointers to NULL
	length = 0;									//increased by "add" function
	done = d;									//Number is determined
	add(data);									//add first element
}


List::~List(){									//Destructor, deallocates memory
	destroy();
}


void List::add(int x){							//Add data to list
	Node * newNode = new Node(x);				//the new data to add
	length++;									//one node more
	if (type == LIFO){							//LIFO list, add to beginning of list
		newNode->setNext(first);				//the new node points to first
		first = newNode;						//first points to the new node
		if (length == 1)						//first to be added
			last = first;						//used in remove function
	}
	else if (type == FIFO){						//FIFO list, add to the end
		if (!first) first = newNode;			//first == NULL
		if (!last) last = newNode;				//last == NULL
		else{									//Not first entry
			last->setNext(newNode);				//last entry points to new node
			last = newNode;						//last points to the new node
		}
	}
	else if (type == ASYNC){					//ASYNC-list, sorted inceasing order
		Node* current = first;					//where to add the new node, start at beginning

		if (!first || *newNode < first){		//first entry, or smaller than first (least) entry
			newNode->setNext(first);			//new node points to first node
			first = newNode;					//first points to new node, (adds to beginning)
		}
		else{									//put new entry between two other
			while (current->getNext() && *newNode > current->getNext())	//not last and new node > the next
				current = current->getNext();	//point to next entry
			newNode->setNext(current->getNext());//new node points to next entry
			current->setNext(newNode);			//previous entry points to new entry
		}

		current = first;						//start at beginning
		while (current->getNext())				//find last element
			current = current->getNext();		//jump to next node
		last = current;							//set last (used in remove)

	}
	else if (type == DESYNC){					//DESYNC-list, sorts in decreasing order
		Node* current = first;					//		SAME AS ABOVE
		if (!first || *newNode > first){		//		ONLY DIFFERENCE IS > INSTEAD OF <
			newNode->setNext(first);
			first = newNode;
		}
		else{									//		AND < INSTEAD OF >
			while (current->getNext() && *newNode < current->getNext())
				current = current->getNext();
			newNode->setNext(current->getNext());
			current->setNext(newNode);
		}

		current = first;						//start at beginning
		while (current->getNext())				//find last element
			current = current->getNext();		//jump to next node
		last = current;							//set last (used in remove)

	}
	else{										//Wrong listtype specified, write error and deallocate memory for new node
		cout << "\n\n\tERROR: \tINCOMPATIBLE LIST TYPE!\n\n";
		delete newNode;
		length--;
	}
}


void List::display(){							//Write list to screen
	Node * np = first;							//Current node to write
	cout << ((done) ? "DONE:\t" : "\t");
	while (np){									//node exists
		cout << np->getData() << ' ';			//Display data
		np = np->getNext();						//Hop to next
	}
}


int  List::getLength(){							//get length of list
	return length;
}

int  List::getData(){							//Returns the value of field if only one is present
	return (done) ? first->getData() : 0;
}


int  List::getData(int index){					//Returns the value of Node at index
	int counter = 1;							//index of current element
	Node *current = first;						//start at first node
	if (index <= length){						//if index is present
		while ((counter < index) && current->getNext()){	//Get pointer to node
			current = current->getNext();
			counter++;
		}
		return current->getData();				//return requested data
	}
	return 0;									//return 0 if fault
}


void List::setData(int index, int val){			//Update data at index
	int counter = 1;							//index of current element
	Node *current = first;						//start at first node
	if (index <= length){						//if index is present
		while ((counter++ < index) && current->getNext())	//Get pointer to node
			current = current->getNext();
		current->setData(val);					//update value
	}
	else
		cout << "\n\n\tERROR: Couln't find Node!\n\n";
}


void List::remove(int data){					//Removes items that == "data" from the list
	Node * current = first, *np = current->getNext();
	
	while (current){
		if ((current == first) && (current->getData() == data)){		//first node and data match
			first = np;													//set first to fist->next
			delete current;						//delete node
			length--;							//decrease length of list
			current = np;						//set current to next
			np = np->getNext();					//set next to next
		}
		else if ((np == last) && (np->getData() == data)){	//last node and data match
			last = current;						//move last to prevoius
			current->setNext(np->getNext());	//set previous to point to NULL
			delete np;							//delete last
			length--;							//decrease length of list
			np = NULL;							//No next, set to NULL
		}
		else if (np && np->getData() == data){		//Not first or last, next has data match
			current->setNext(np->getNext());	//set previous to next->next
			delete np;							//delete next
			length--;							//decrease length of list
			np = current->getNext();			//set next to next node
		}
		current = np;							//Jump to next
		if(np) np = np->getNext();				//get next, if not last
	}
}


bool List::isSet(int data){						//returns true if "data" is in list
	Node *np = first;							//Current node
	while (np){									//Current node points to another node
		if (np->getData() == data) return true;	//Check data
		np = np->getNext();
	}
	return false;								//return false if no match
}


bool List::isDone(){							//Returns true if the number is determined
	return done;
}


void List::setDone(){							//Specifies number as done
	if(length == 1) done = true;
	else cout << "\n\n\tERROR: Can't set done if length != 1\n\n";
}


void List::destroy(){							//Removes ALL items in list
	Node * np = first->getNext();				//np = next ptr, next address
	while (np){									//still a next-address
		delete first;							//Delete first entry
		first = np;								//next-address is now the first
		np = np->getNext();						//get next
	}
	delete first;								//delete first (last left)
	first = NULL;								//reset varables
	last = NULL;
	length = 0;
	done = false;
}