#pragma once											//Ensures only compiled once

#define DEBUG false										//enable/disable debug-mode

#include <iostream>										//cin, cout
#include "List.h"										//List


using namespace std;	
const int BOARDSIZE = 9;								//the board is N x N numbers, with all possible values 1 - N ( 0 = no value)

class Board{											//Instance of sudokuboard
private:
	List *board[BOARDSIZE][BOARDSIZE];					//the board with all possible values for each field
	int numLeft;
public:
	Board();											//initiates the board with default values
	~Board();											//Deletes all lists in array

	void newBoard();									//get numbers from user
	void display();										//prints board

	bool isInRow(const int y, const int data);			//returns true if number is in the same row
	bool isInCol(const int x, const int data);			//returns true if number is in the same column
	bool isInBlock(const int y, const int x, const int data);//returns true if number is in the same block
	void blockBuster();									//Checks each block and assigns values that has only one spot
	void rowBuster();									//Checks each row and assigns values that has only one spot
	void colBuster();									//Checks each column and assigns values that has only one spot


	void calculatePossibilities();						//Generates a list of possible values for each field
	void solve();										//solves the puzzle

	void getBlockCoords(const int y, const int x,
		int & y1, int & y2, int & x1, int & x2);		//Specifies the block for a given coordinate
	void test();										//TEstfunction: fills the array with a board
};
