//SudokuSolver.cpp
//A program that solves sudoku
#include <iostream>
#include "Board.h"
using namespace std;

int main(){
	Board *b;
	char temp;

	do{
		b = new Board;

		cout << "Test (T) or manual plot (M) ? ";
		cin >> temp;
		temp = toupper(temp);

		switch (temp)
		{
		case 'T':
			b->test();
			break;
		case 'M':
			b->newBoard();
			b->solve();
			break;
		default:
			cout << "\n\nInvalid input!\n\n";
			break;
		}
		delete b;
		cout << "New board? [y/n]: ";
		cin >> temp;
		temp = toupper(temp);
	} while (temp != 'N');

	return 0;
}